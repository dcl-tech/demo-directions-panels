/////////////////////////////////////////////////////////
// Directions demo - by Carl Fravel
// Demonstrates the right coordinates for thin boxes and planes for them to serve as properly-oriented display surfaces
// Stand INSIDE  the circle of 8 pairs of panels to see the direction that the faces are facing
// For example, The panel pair farthest north shows a south-facing image as seen from the center, hence is "S."
//
// Also demonstrates and allows testing of the required standoff distance 
// of a textured thin box or plane from the object behind it to avoid flicker.
// 

/////////////////////////////////////////////////////////

import {spawnEntity, spawnPlaneX} from './modules/SpawnerFunctions'
import {DirectionsPanelsDisplay} from './modules/DirectionsPanelsDisplay'

//////////////////////////////////////////
// Create Initial Scene Entities

// This scene entity wraps the whole scene, so it can be moved, rotated, or sized as may be needed
const scene = spawnEntity(0,0,0, 0,0,0, 1,1,1)

// Ground Plane
//const grassyFineTexture = new Texture("materials/Tileable-Textures/grassy-512-1-0.png")
const grassyFineMaterial = new Material()
//grassyFineMaterial.albedoTexture = grassyFineTexture
grassyFineMaterial.albedoColor = new Color3(0, 0.5, 0)
const groundPlane = spawnPlaneX(8,0,8,    90,0,0,    16,16,1)
groundPlane.addComponent(grassyFineMaterial)
groundPlane.setParent(scene)

// Now instantiate the directions panels display
// You can adjust the optional DF parameter to mess with the standoff gap to test flicker at various distances from the scene
// You can turn off the backingPanels if you wish to test pixelization of the textures without the backing panels.
const dpd:Entity = new DirectionsPanelsDisplay(0.01, true)
dpd.setParent(scene)
